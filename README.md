# TV-Listener
## Description
This is the "listener" component of the _Epitech Strasbourg TV system_.

## Specifications
This micro-service has a single routè : `/listener`, which when used with a `POST` method containing the JSON data for a new composition, extracts the necessary static ressource ids and passes it to the front-end ressource manager before emitting a _socket.io_ `refresh` event to the connected clients.

## Author
Victor « Magic Bit-Bot » Schubert
