var server = require('./server.js');
var fs = require('fs');
var http = require('http');
var io = require('socket.io')(server.http);
var config = require('../configs/config.js');
var express = require('express');
var moment = require('moment');
var router = express.Router();

router.post('/', function(req, res) {
    var ids;
    var ressourceRequest;

    console.log('[' + moment().toISOString() + '] New composition incoming.');
    ids = getIds(req.body);
    ids = JSON.stringify(ids);
    ressourceRequest = http.request({
        host: config.hosts.ressourceManager.name,
        port: config.hosts.ressourceManager.port,
        method: 'PUT',
        path: '/assets'
    }, function(ressourceResponse) {
        if (ressourceResponse.statusCode !== 200) {
            var errString = 'Ressource manager failed to retrieve ressources ' + ids + '.';
            console.error(errString);
            res.writeHead(500, {'Content-Type':'text/plain'});
            res.end(errString);
        } else {
            fs.writeFile(config.paths.composition, JSON.stringify(req.body, null, '\t'), {mode: 420}, function(err) {
                if (err) {
                    var errString = 'Failed to write composition to file: ' + err;
                    console.error(errString);
                    res.writeHead(500, {'Content-Type':'text/plain'});
                    res.end(errString);
                } else {
                    io.emit('refresh');
                    res.writeHead(200, {'Content-Type':'text/plain'});
                    res.end();
                }
            });
        }
    });
    ressourceRequest.on('error', function(err) {
        var errString = 'Failed to contact the ressource manager: ' + err.message;
        console.error(errString);
        res.writeHead(500, {'Content-Type':'text/plain'});
        res.end(errString);
    });
    ressourceRequest.end(ids);
});

module.exports = router;

function getIds(composition) {
    var ids = [];
    getIdsRec(composition, ids);
    return ids;

    function getIdsRec(module, ids) {
        if (module.type === 'video' || module.type === 'picture') {
            if (module.config.id) {
                ids.push(module.config.id);
            } else {
                console.warn('"' + module.type + '" module ' + module.id + ' has no ressource ID.');
            }
        }

        if (module.sub_module && module.sub_module.constructor === Array) {
            var i;
            for (i in module.sub_module) {
                getIdsRec(module.sub_module[i], ids);
            }
        }
    }
}
