'use strict';

var express = require('express');
var http = require('http');
var app = express();
var server = http.Server(app);

module.exports = {
    'app': app,
    'http': server
};
