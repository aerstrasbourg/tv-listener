module.exports = {
    hosts: {
        listener: {
            name: '0.0.0.0',
            port: 31415
        },
        ressourceManager: {
            name: '127.0.0.1',
            port: 27182
        }
    },
    paths: {
        composition: 'composition.json'
    }
};