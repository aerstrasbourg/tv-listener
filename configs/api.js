var server = require('../lib/server.js');
var bodyParser = require('body-parser');
var app = server.app;

exports.app = app;

var listenerRouter = require('../lib/listener.js');
// var router = require('../lib/controller.js');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/listener', listenerRouter);
// app.use('/route', router);
