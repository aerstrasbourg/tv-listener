var api = require('./configs/api');
var config = require('./configs/config')

api.app.listen(config.hosts.listener.port, config.hosts.listener.name, function() {
    console.log('Listening on ' + config.hosts.listener.name + ':' + config.hosts.listener.port);
});
